package com.oaker.hours.service;

import com.baomidou.mybatisplus.service.IService;
import com.oaker.hours.doman.entity.MhArchiveHour;

import java.util.List;

public interface MhArchiveHourService   extends IService<MhArchiveHour>  {
    void saveBatch(List<MhArchiveHour> mhArchiveHourList);

    List<MhArchiveHour> queryByArchiveId(Long id);
}
