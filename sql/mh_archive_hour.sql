/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : wuyu_cost

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 17/12/2023 18:30:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for mh_archive
-- ----------------------------

DROP TABLE IF EXISTS `mh_archive`;
CREATE TABLE `mh_archive`  (
  `id` bigint(20) NOT NULL,
  `dept_id` bigint(20) NULL DEFAULT NULL,
  `archive_date` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `create_user` int(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;



-- ----------------------------
-- Table structure for mh_archive_hour
-- ----------------------------
DROP TABLE IF EXISTS `mh_archive_hour`;
CREATE TABLE `mh_archive_hour`  (
  `archive_id` bigint(20) NOT NULL,
  `hour_id` bigint(20) NOT NULL,
  PRIMARY KEY (`archive_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Fixed;

SET FOREIGN_KEY_CHECKS = 1;





INSERT INTO `wuyu_cost`.`sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2087, '归档管理', 2073, 4, 'archived', 'mhmanager/mharchived/index', 1, 0, 'C', '0', '0', '', 'documentation', 'admin', '2023-12-17 16:18:42', 'admin', '2023-12-17 16:19:13', '');




-- ----------------------------
--  更新填报记录的路由规则
-- ----------------------------


UPDATE `wuyu_cost`.`sys_menu` SET `menu_name` = '填报记录', `parent_id` = 2002, `order_num` = 2, `path` = 'fillInWorkingHours', `component` = 'workingHours/myWorkingHours/fillInWorkingHours', `is_frame` = 1, `is_cache` = 0, `menu_type` = 'C', `visible` = '0', `status` = '0', `perms` = 'mh:hour:stat', `icon` = 'server', `create_by` = 'admin', `create_time` = '2021-09-01 02:22:00', `update_by` = 'admin', `update_time` = '2023-12-17 16:02:24', `remark` = '' WHERE `menu_id` = 2004;
